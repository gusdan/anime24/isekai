
    async function initializePlayer(id, url, drmKey, enableCustomConfig) {
      const video = document.getElementById(id);
      const ui = video['ui'];
      const controls = ui.getControls();
      const player = controls.getPlayer();    
      const config = {
        'controlPanelElements': ['time_and_duration','playback_rate','mute','spacer','captions','language' ,'quality','fullscreen'],
        'playbackRates': [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
        'seekBarColors': {base: 'rgba(255,255,255,.2)', buffered: 'rgba(255,255,255,.4)', played: 'rgb(255,0,0)'} 
      };
      ui.configure(config);
      player.configure({drm: {clearKeys: drmKey}});
      window.player = player;
      window.ui = ui;
      player.addEventListener('error', onPlayerErrorEvent);
      controls.addEventListener('error', onUIErrorEvent);
      if (enableCustomConfig) {
        player.configure('manifest.dash.ignoreMinBufferTime', true);
       
        player.configure('streaming.rebufferingGoal', 3 /* second */);
      }
      try {
        await player.load(url);
        console.log('The video has now been loaded!');
      } catch (error) {
       
onPlayerError(error);
      }
    }

    function onPlayerErrorEvent(errorEvent) {
      onPlayerError(errorEvent.detail);
    }

    function onPlayerError(error)
{
      console.error('Error code', error.code, 'object', error);
    }

    function onUIErrorEvent(errorEvent) {
      onPlayerError(errorEvent.detail);
    }

    function changePlayer(Id) {
  initializePlayer('bein1my', 'https://pop5clustera00de07172379a62d6189.hypp.tv:443/PLTV/88888888/224/3221227927/3221227927.mpd?rrsip=web.hypp.tv:443&zoneoffset=0&servicetype=1&icpid=&accounttype=1&limitflux=-1&limitdur=-1&accountinfo=U0v281lovZMLWzqtXjPtYuOXwQCoIQRk449J%2BBUCcawgQY43Tg5eLk6%2BKHkOBbkVIq%2FzwD5xYAe8dI3PltEQ%2BUrcymHBIhx5oJP4jv2fPK0%3D%3A20230206101746%3AUTC%2C1003663983%2C115.164.187.20%2C20230206101746%2Curn:Huawei:liveTV:XTV55456338%2C1003663983%2C-1%2C0%2C1%2C%2C%2C2%2C593%2C%2C%2C2%2C1343117%2C0%2C248412%2C47562943%2C%2C%2C2%2C1%2CEND&GuardEncType=2&it=H4sIAAAAAAAAADWOQY_CIBSE_w1HApSqPXByY2Jiqol1r-YBD2xKi0I18d_brt25vZn5XmZMYHD_o4oNc9pVVsvSSF45raWoDCvEdBQlSpLxUUcliIEQ2sHX0c7Y73l75YwyKgSnXJBmfrcL4Jdm_ew1JlX8Y2dMr9agstnRF2QK3if0MLZxoKcA70sKS4Vgs0zjq3W5qpicJNZknN0Gcjcl5AZ5G_s7JLSH6P8A5SBkJHcwHXisoUc1PEP4csdkpzUfwVvnv_QAAAA&tenantId=6001', {'8e5c88c1ad411ce4aa1fcd3e63fa9448': 'fb4e6d3c2a444c3711477438bcc0b5fc'});
      
  initializePlayer('soccerch', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/soccerchannel-test/sa_dash_vmx/soccerchannel-test.mpd', {'4d38060bf41b3c29df0ec950ece6b5da': '7ee9506b13480491d79b71c062ab5366'});
      
   initializePlayer('ss1', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports-HD/sa_dash_vmx/MNCSports-HD.mpd', {'531c6d50e3e9f9ba66446f624f492289': 'd769d9ae238bdd424f8bcdcdc9a3801f'});
      
  initializePlayer('ss2', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports2-HD/sa_dash_vmx/MNCSports2-HD.mpd', {'45fec91ce1f19b6b1f31d69dcfaaf6cd': '843e228ab109e9aa6c4822ee4ad05d7d'});
      
  initializePlayer('ss3', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports3-HD/sa_dash_vmx/MNCSports3-HD.mpd', {'fadd2720deff5925ab86df0819cd7775': 'f67ff08c7ebc779f6a6fcfc83835f65b'});
      
  initializePlayer('ss3sd', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Soccer-2/sa_dash_vmx/Soccer-2.mpd', {'fadd2720deff5925ab86df0819cd7775': 'f67ff08c7ebc779f6a6fcfc83835f65b'});    
  initializePlayer('ss4', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Sportstar4/sa_dash_vmx/Sportstar4.mpd', {'b81b1942c65f35547ced1bff074206a5': 'a07dc4d86f51db7195f4b7e2abe9d9e8'});
  
  initializePlayer('spotv', 'https://atemecdnbalancer-voe.sysln.id/live/eds/SPOTVHD/mpd/SPOTVHD.mpd', {'e60ece8f0d9042fcb52508055ec48e5e': '213f438bd4961cda987d41b7f154f1e5'});
  
  initializePlayer('spotv2', 'https://atemecdnbalancer-voe.sysln.id/live/eds/SPOTV2HD/mpd/SPOTV2HD.mpd', {'e6ed3fdf6e9f491d9ead109fc0b00cfc': '3bc6c45722eb5fa7b343de9bffc4f7c7'});
  
  initializePlayer('fight', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/FightSports/sa_dash_vmx/FightSports.mpd', {'d2159ffe3be22ed4916a6abe4d58d265': '3e4efcec08d5d5c18a403b7048a43638'});
  
  
     //NASIONAL
     
   initializePlayer('oktv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/OKTV/sa_dash_vmx/OKTV.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
      
  initializePlayer('mnctv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCTV-HD/sa_dash_vmx/MNCTV-HD.mpd', {'828e0aba9825c3546a2831e4c0c36f7f': 'f85d3dcd38981368ab3da597e97ebdcc'});
      
   initializePlayer('rcti', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/RCTI-DD/sa_dash_vmx/RCTI-DD.mpd', {'9ba3e153ef8956d6e2b0684fcf74f58f': 'dbc28cb5c6426080f984a5b6d436bb30'});
      
  initializePlayer('gtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/GTV-HD/sa_dash_vmx/GTV-HD.mpd', {'88f6c7cbd793374cb5f12d7e26dcd63b': 'e82daa7c7bfb03d99327463fdbd37336'});
      
  initializePlayer('inews', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/iNewsTV-HDD/sa_dash_vmx/iNewsTV-HDD.mpd', {'a31cf5136789514b7e12b9cc99307c84': '980e54d671ffc2b2f4cf54e75cae0ac2'});
      
  initializePlayer('trans7', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Trans7-2/sa_dash_vmx/Trans7-2.mpd', {'26bd60e03bbed7819eea0b27075a1897': 'd221ba6a6ab66e3083c001d2c4a3e5c5'}); 
  
  initializePlayer('transtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/TransTV-2/sa_dash_vmx/TransTV-2.mpd', {'334950781f60a66f5e58324b70ac675d': '73caca97be8999ce9cfeb3babad8669d'});
  
  initializePlayer('antv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/ANTV/sa_dash_vmx/ANTV.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('tvone', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/TVOne/sa_dash_vmx/TVOne.mpd', {'90204c05545f95a262bb0d3ac45de870': '0e80f2a19fdf8da476bf695cd9570bb2'});
  
  initializePlayer('metrotv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Metro-TV2/sa_dash_vmx/Metro-TV2.mpd', {'4497473c5b978655ce62de7873e04174': '6af319f8d82351f8c3b18c9abdfdef4d'});
  
  initializePlayer('tvri', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/PemersatuBangsa/sa_dash_vmx/PemersatuBangsa.mpd', {'ca65af90adc5e3ddb180e16426bb67da': 'b6f87a3a128dbd75ead036f596edeae7'});
  
  initializePlayer('kompas', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/KompasTV/sa_dash_vmx/KompasTV.mpd', {'493fd10e609dd02ae97964f438e7e530': '695cf9896182c52f1c3a25820e7778f5'});
  
  initializePlayer('rtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/RTV/sa_dash_vmx/RTV.mpd', {'30384ee5424eab4afdff34d7a59e0ef9': '5f3d2f2c194266ae9a28210f8976cbe0'});
  
  initializePlayer('net', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/NetTV-HD/sa_dash_vmx/NetTV-HD.mpd', {'6b7bbcf1d511a56e6b3ceda392e4fa33': 'd1766244d7c1c44efd4c67aafae3ee7b'});
  
  initializePlayer('jaktv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/JakTV/sa_dash_vmx/JakTV.mpd', {'f2c9087e5432ce2c2f4ae8e1fd77ae4c': 'f7a50262991ca7d505fce1572a7f0c2a'});
   
      initializePlayer('jtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/JTV/sa_dash_vmx/JTV.mpd', {'10e203f655014b5d18b0c85a6c72c809': 'acdcd0b65014e4929c6296171bdfebad'});
      
   initializePlayer('jtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/BaliTV/sa_dash_vmx/BaliTV.mpd', {'c6bb2f182bc8c5905c2636153ac36249': '152b81c5cd1306bb83993290193063cb'});
   
   initializePlayer('daaitv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/DAAITV/sa_dash_vmx/DAAITV.mpd', {'eb7c09e964e89e668266ea31f2710ffa': '0a9832e32206fff46674b8210c9b6897'});
   
   initializePlayer('bandungtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/BandungTV/sa_dash_vmx/BandungTV.mpd', {'a7fd43751f24d64a16f6f4d289ebd395': '2ef65773d541ccaf8ba61a3d141711f0'});
  
  // MOVIES
   initializePlayer('tvn_movies', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/tvNMovies/sa_dash_vmx/tvNMovies.mpd', {'45c004003b09719751adb885d28d491f': 'fe82bc3ffe00477c833812fae74caed7'});
      
  initializePlayer('fmn', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/FMN/sa_dash_vmx/FMN.mpd', {'009a1278e0755ed82e70df01460e90c3': '12d010a918431785676c4fd63ef648bd'});
      
   initializePlayer('hits_movies', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/HitsMovies/sa_dash_vmx/HitsMovies.mpd', {'5d9684b1737e3b68801b4cff7225d4bb': '902e7634820c26a8ff36f3708f0191d4'});
      
  initializePlayer('imc', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/IndonesiaMovieChannels-HD/sa_dash_vmx/IndonesiaMovieChannels-HD.mpd', {'1a551e30ef88a5a121dcea6a74a3aee7': 'b0f3de006d6e31e967a5bc41be086e64'});
      
  initializePlayer('galaxy', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Galaxy-HD/sa_dash_vmx/Galaxy-HD.mpd', {'e5c40292f8d01b1d5f0c6b3904d73104': 'bcb2ac12e11c69594ba217bfe8714efe'});
      
  initializePlayer('galaxy_premium', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/GalaxyPremium-HD/sa_dash_vmx/GalaxyPremium-HD.mpd', {'7bc29ff5f405dff971e3a326223fe26c': '06849a953a38da997b31bacf433cc74a'});    
  initializePlayer('thrill', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Thrill/sa_dash_vmx/Thrill.mpd', {'06e7e95fdb30086d24111f300c9d91f1': '9431050f760f692bfd396bbd84cb5161'});
  
  initializePlayer('cinemaword', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/CinemaWorld/sa_dash_vmx/CinemaWorld.mpd', {'958df746f24f371dffc95e7cd714fbd7': 'fbd6e4540b9a917a27da8be01542f53f'});
  
  initializePlayer('celestial_movies', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/CelestialMovie/sa_dash_vmx/CelestialMovie.mpd', {'13aecb26aeee22bc86920045f22d134d': '80aa44d998c7c0cc221c96d26730fe9b'});
  
  initializePlayer('celestial_clasic_movies', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/CelestialClassic/sa_dash_vmx/CelestialClassic.mpd', {'12da619438b3748da206142216d0943b': '6958377109d34561ad20645319cec62c'});
  
   initializePlayer('zee_bioskop', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/ZeeBIOSKOP/sa_dash_vmx/ZeeBIOSKOP.mpd', {'9e7df70530006bef067327cefde82c0e': 'b92b6a2aba522bd9bb2d07cf241e7398'});
      
  initializePlayer('axn', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/AXN/sa_dash_vmx/AXN.mpd', {'c5d5297a84f8b2de977f07286345c591': 'bdb773d6d25303b75a396b8040cfaf8c'});
      
   initializePlayer('cinemachi', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-HD/sa_dash_vmx/Cinemachi-HD.mpd', {'eeea0c2986e91a13eacd590ad1ebfe43': '0acc16bd84921489f67bad19e4099cae'});
      
  initializePlayer('cinemachi_action', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Action/sa_dash_vmx/Cinemachi-Action.mpd', {'fba45325e2cd8355972ede4981f43b2a': 'd88a4bdd67f89cd21eb074c81de1b994'});
      
  initializePlayer('cinemachi_kids', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Kids-HD/sa_dash_vmx/Cinemachi-Kids-HD.mpd', {'ebe6190a3cfbbed4ec34e4d9c7a30e1e': 'd161b1f737e2aee9501149406c8abe9e'});
      
  initializePlayer('cinemachi_max', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Max-HD/sa_dash_vmx/Cinemachi-Max-HD.mpd', {'6c256f70830647e4bdf654d86e710ed5': 'fe41e9bcbae52f9d233407f7488bca5f'}); 
  
  initializePlayer('cinemachi_xtra', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Xtra-HD/sa_dash_vmx/Cinemachi-Xtra-HD.mpd', {'b64eee6360b118d0d90329cc2c9e4c60': '4e97aa6422b26ffb372647dbd0a7683c'});
  
  initializePlayer('my_cinema', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MyCinema/sa_dash_vmx/MyCinema.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('my_cinema_asia', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MyCinema-Asia/sa_dash_vmx/MyCinema-Asia.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('my_family', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MyFamily/sa_dash_vmx/MyFamily.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  
  //entertainment
  
  
  
  initializePlayer('rock_entertainment', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Rockentertainment/sa_dash_vmx/Rockentertainment.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
  
  initializePlayer('mtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MTV/sa_dash_vmx/MTV.mpd', {'5b9753643572b0c6c467793b50900029': 'ebeda1cf36dae2b0bdedf1065129ea93'});
  
  initializePlayer('mtv90', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MTV-90-HD/sa_dash_vmx/MTV-90-HD.mpd', {'37ef71f668c1c1126595be0f557587f5': '008df022d78a9ec318a745cf8365b048'});
  
  initializePlayer('celebtv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCInfotainment/sa_dash_vmx/MNCInfotainment.mpd', {'a17bbb3d95ba22f0fa680bba609340f6': 'cb682391dc1830d2cec8ca9d4ce681cc'});
  
  initializePlayer('ent', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCEntertainment/sa_dash_vmx/MNCEntertainment.mpd', {'e97187c5c7978ad9fc48943df7ed9045': '36af3adb3e7c4edd9286e7412fc5f596'});
  
  
  
  initializePlayer('vprime', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/VisionPrime/sa_dash_vmx/VisionPrime.mpd', {'50056951f6d5f5fbaf286cd99c965379': '4ec78e3b7d66d1db3815a127f792b604'});
  
  initializePlayer('musictv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MusicChannel/sa_dash_vmx/MusicChannel.mpd', {'0bea88ab972e06760a04712cc423d841': '4039d607f88f1b1f677e4beb6ee66637'});
  
  initializePlayer('tvn', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/tvN/sa_dash_vmx/tvN.mpd', {'742131dbcb507c31502eb60b32be6bb8': '209f03c525bbbc9fd8652aa7a9cc5cb0'});
  
  initializePlayer('hits', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/HITS/sa_dash_vmx/HITS.mpd', {'78d17d4851a5e9eede96f283b15ec053': 'c5ae33f70cc967fe107b35eb7225f52a'});
  
  initializePlayer('kix', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/KIX/sa_dash_vmx/KIX.mpd', {'fe761e8fac143d40510e70825dad0b20': 'eed9fb0b4e254e9104ef98e8a7035387'});
  
  initializePlayer('kix', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Rockaction/sa_dash_vmx/Rockaction.mpd', {'b9c123fbf587e6524b2706bd960c7f67': 'd2aaac6845b33627cdce1272feac5acc'});
 
  
  //pengetahuan
  initializePlayer('outdoorchannel', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/outdoorchannel/sa_dash_vmx/outdoorchannel.mpd', {'05457a5e16a76fe666a22cc7ae2c6548': '626e37901bfb985e994d714e9f9de1b5'});
  
  initializePlayer('lovenature', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/lovenature/sa_dash_vmx/lovenature.mpd', {'c3066eaa0984588f78d12dcdb75ef88b': '4e98eb4927941bff51ea526d223fa16d'});
  
  initializePlayer('NHKWorldTV', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/NHKWorldTV/sa_dash_vmx/NHKWorldTV.mpd', {'62ce01c83653c3f20af6bcafc6979fe6': 'f1325e71f440c06f7e93d7551cb4014a'});
  
  initializePlayer('BBCEarth', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/BBCEarth-HD/sa_dash_vmx/BBCEarth-HD.mpd', {'c154c12b5f8336cc9cbef82fb5cc0ab4': '5c365427c75eacf217255effcb5fff3e'});
  
  initializePlayer('History', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/History/sa_dash_vmx/History.mpd', {'2ccaa7d9731c2e69fa5344c5a547b836': 'd23fb6a3c73eb7ab8ad19ca841db0fb5'});
  
  initializePlayer('CrimeInvestigation', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/CrimeInvestigation/sa_dash_vmx/CrimeInvestigation.mpd', {'dc6157edc80fa15e6b6f23e695d461e4': '7c068dd99c87e0ab1efc635bb77adcb4'});
  
  initializePlayer('CGTN_Doc', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/CGTN_Doc/sa_dash_vmx/CGTN_Doc.mpd', {'87354c602dcabe0eef472f2a68f6685a': '518ecaa33b9f05494f8cfaa7dd56a968'});
  
  //KIDS 
  initializePlayer('babytv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/BabyTV-NewHD/sa_dash_vmx/BabyTV-NewHD.mpd', {'2fc62ba18b44dce828a5fb8a213d6e5b': '02a01282f77edf03e964e713fb447e93'});
  
  initializePlayer('kidstv', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/KidsChannel/sa_dash_vmx/KidsChannel.mpd', {'17340c4223a4674b1b7da8d8d9e11174': '11a73aad2c75ddf338840b08fb915993'});
  
  initializePlayer('nickeloden', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Nickelodeon/sa_dash_vmx/Nickelodeon.mpd', {'785024598882dc751652cea2bce44b8e': '56a40e58acd59965e865947dd719108d'});
  
  initializePlayer('animax', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Animax/sa_dash_vmx/Animax.mpd', {'6344a8272809245e3fa9d926099528c3': '93b6c4ff420c6864a6294f6d223d9b2c'});
  
  initializePlayer('my_kids', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/My-Kids/sa_dash_vmx/My-Kids.mpd', {'a2dd864fc95998efd52979e5f732e029': '4f705ca934ba3ef5513fea618d4f938d'});
  
  initializePlayer('cebebies', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Cbeebies/sa_dash_vmx/Cbeebies.mpd', {'194891b41223a5f1ec2beff1c95e2f53': 'eeb1fb2ec011f9563653495f9ac427b1'});
  
  initializePlayer('nick_jr', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/NickJr-HDD/sa_dash_vmx/NickJr-HDD.mpd', {'f71ca09f57ea09e27ee63d7d1f0d0e31': '8a07ef45b4a18973ef12f8da482dcec0'});
  
  initializePlayer('My_Kids', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/My-Kids/sa_dash_vmx/My-Kids.mpd', {'a2dd864fc95998efd52979e5f732e029': '4f705ca934ba3ef5513fea618d4f938d'});
  
  initializePlayer('Zoomoo', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Zoomoo/sa_dash_vmx/Zoomoo.mpd', {'5ff6154f28d6950063f9f5b4db7be009': '93cac38fd15124acce5b658896df0a25'});
  
  initializePlayer('Dreamwork', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/Dreamwork-HD/sa_dash_vmx/Dreamwork-HD.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
  
  
  
  //berita
  
  initializePlayer('MNCnews', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/MNCnews-HDD/sa_dash_vmx/MNCnews-HDD.mpd', {'26cc3e3343858d3986322f16e10b5823': '0ad735e707e1c79bc550efa49f2ea4bd'});
  
  initializePlayer('BeritaSatu', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/BeritaSatu/sa_dash_vmx/BeritaSatu.mpd', {'d022ff3be9391a16a8436261059806b0': 'e6dd32d6a257f900acb52a82d898bb81'});
  
  initializePlayer('IDX', 'https://xxx.livestreamingbolatv1.workers.dev/https://cors-proxy.elfsight.com/xxx.freemiumiptv.top/mnclokal/live/eds/IDX/sa_dash_vmx/IDX.mpd', {'fbdbe252e6354b175852d1929c42d848': '7a8bb14fcf8fd0b896ab41c29a78aae0'});
      
      
      
      
     //batas akhir
   }document.addEventListener('shaka-ui-loaded', function() {
    initializePlayer('vlive', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/VPlusLIVE/sa_dash_vmx/VPlusLIVE.mpd', {'fadd2720deff5925ab86df0819cd7775': 'f67ff08c7ebc779f6a6fcfc83835f65b'});
      
  initializePlayer('soccerch', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/soccerchannel-test/sa_dash_vmx/soccerchannel-test.mpd', {'4d38060bf41b3c29df0ec950ece6b5da': '7ee9506b13480491d79b71c062ab5366'});
 
  initializePlayer('soccerch2', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/rcti-soccerchannel-hd/sa_dash/rcti-soccerchannel-hd.mpd', {'ecbc9e6fe6b145efb6658fb5cf7427f8': '03c17e28911f71221acbc0b11f900401'}); 
  
  initializePlayer('ss1', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports-HD/sa_dash_vmx/MNCSports-HD.mpd', {'531c6d50e3e9f9ba66446f624f492289': 'd769d9ae238bdd424f8bcdcdc9a3801f'});
      
  initializePlayer('ss2', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports2-HD/sa_dash_vmx/MNCSports2-HD.mpd', {'45fec91ce1f19b6b1f31d69dcfaaf6cd': '843e228ab109e9aa6c4822ee4ad05d7d'});
      
  initializePlayer('ss3', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCSports3-HD/sa_dash_vmx/MNCSports3-HD.mpd', {'fadd2720deff5925ab86df0819cd7775': 'f67ff08c7ebc779f6a6fcfc83835f65b'});
      
  initializePlayer('ss3sd', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Soccer-2/sa_dash_vmx/Soccer-2.mpd', {'fadd2720deff5925ab86df0819cd7775': 'f67ff08c7ebc779f6a6fcfc83835f65b'});    
  
  initializePlayer('ss4', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Sportstar4/sa_dash_vmx/Sportstar4.mpd', {'b81b1942c65f35547ced1bff074206a5': 'a07dc4d86f51db7195f4b7e2abe9d9e8'});

  initializePlayer('ss4fhd', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/VplusLiveRplus/sa_dash/VplusLiveRplus.mpd', {'b81b1942c65f35547ced1bff074206a5': 'a07dc4d86f51db7195f4b7e2abe9d9e8'});
  
  initializePlayer('spotv', 'https://atemecdnbalancer-voe.sysln.id/live/eds/SPOTVHD/mpd/SPOTVHD.mpd', {'e60ece8f0d9042fcb52508055ec48e5e': '213f438bd4961cda987d41b7f154f1e5'});
  
  initializePlayer('spotv2', 'https://atemecdnbalancer-voe.sysln.id/live/eds/SPOTV2HD/mpd/SPOTV2HD.mpd', {'e6ed3fdf6e9f491d9ead109fc0b00cfc': '3bc6c45722eb5fa7b343de9bffc4f7c7'});
  
  initializePlayer('fight', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/FightSports/sa_dash_vmx/FightSports.mpd', {'d2159ffe3be22ed4916a6abe4d58d265': '3e4efcec08d5d5c18a403b7048a43638'});
  
  
     //NASIONAL
     
   initializePlayer('oktv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/OKTV/sa_dash_vmx/OKTV.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
      
  initializePlayer('mnctv', 'https://rplus.xyssatuu.workers.dev/mnctv.rctiplus.id/mnctv-sdi.mpd', {'828e0aba9825c3546a2831e4c0c36f7f': 'f85d3dcd38981368ab3da597e97ebdcc'});
      
   initializePlayer('rcti', 'https://rplus.xyssatuu.workers.dev/rcti.rctiplus.id/rcti-sdi.mpd', {'9ba3e153ef8956d6e2b0684fcf74f58f': 'dbc28cb5c6426080f984a5b6d436bb30'});
      
  initializePlayer('gtv', 'https://rplus.xyssatuu.workers.dev/gtv.rctiplus.id/gtv-sdi.mpd', {'88f6c7cbd793374cb5f12d7e26dcd63b': 'e82daa7c7bfb03d99327463fdbd37336'});
      
  initializePlayer('inews', 'https://rplus.xyssatuu.workers.dev/inews.rctiplus.id/inews-sdi.mpd', {'a31cf5136789514b7e12b9cc99307c84': '980e54d671ffc2b2f4cf54e75cae0ac2'});
      
  initializePlayer('trans7', 'https://allo.xyssatuu.workers.dev/https://streaming.indihometv.com/atm/DASH/trans7/manifest.mpd', {'26bd60e03bbed7819eea0b27075a1897': 'd221ba6a6ab66e3083c001d2c4a3e5c5'}); 
  
  initializePlayer('transtv', 'https://allo.xyssatuu.workers.dev/https://streaming.indihometv.com/atm/DASH/transtv/manifest.mpd', {'334950781f60a66f5e58324b70ac675d': '73caca97be8999ce9cfeb3babad8669d'});
  
  initializePlayer('antv', 'https://allo.xyssatuu.workers.dev/https://streaming.indihometv.com/atm/DASH/ANTV/manifest.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('tvone', 'https://allo.xyssatuu.workers.dev/https://streaming.indihometv.com/atm/DASH/tvone/manifest.mpd', {'90204c05545f95a262bb0d3ac45de870': '0e80f2a19fdf8da476bf695cd9570bb2'});
  
  initializePlayer('metrotv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Metro-TV2/sa_dash_vmx/Metro-TV2.mpd', {'4497473c5b978655ce62de7873e04174': '6af319f8d82351f8c3b18c9abdfdef4d'});
  
  initializePlayer('tvri', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/PemersatuBangsa/sa_dash_vmx/PemersatuBangsa.mpd', {'ca65af90adc5e3ddb180e16426bb67da': 'b6f87a3a128dbd75ead036f596edeae7'});
  
  initializePlayer('kompas', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/KompasTV/sa_dash_vmx/KompasTV.mpd', {'493fd10e609dd02ae97964f438e7e530': '695cf9896182c52f1c3a25820e7778f5'});
  
  initializePlayer('rtv', 'https://allo.xyssatuu.workers.dev/https://cdn08jtedge.indihometv.com/dassdvr/130/rtv/manifest.mpd', {'30384ee5424eab4afdff34d7a59e0ef9': '5f3d2f2c194266ae9a28210f8976cbe0'});
  
  initializePlayer('net', 'https://allo.xyssatuu.workers.dev/https://cdn08jtedge.indihometv.com/dassdvr/133/net/manifest.mpd', {'6b7bbcf1d511a56e6b3ceda392e4fa33': 'd1766244d7c1c44efd4c67aafae3ee7b'});
  
  initializePlayer('jaktv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/JakTV/sa_dash_vmx/JakTV.mpd', {'f2c9087e5432ce2c2f4ae8e1fd77ae4c': 'f7a50262991ca7d505fce1572a7f0c2a'});
   
      initializePlayer('jtv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/JTV/sa_dash_vmx/JTV.mpd', {'10e203f655014b5d18b0c85a6c72c809': 'acdcd0b65014e4929c6296171bdfebad'});
      
   initializePlayer('jtv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/BaliTV/sa_dash_vmx/BaliTV.mpd', {'c6bb2f182bc8c5905c2636153ac36249': '152b81c5cd1306bb83993290193063cb'});
   
   initializePlayer('daaitv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/DAAITV/sa_dash_vmx/DAAITV.mpd', {'eb7c09e964e89e668266ea31f2710ffa': '0a9832e32206fff46674b8210c9b6897'});
   
   initializePlayer('bandungtv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/BandungTV/sa_dash_vmx/BandungTV.mpd', {'a7fd43751f24d64a16f6f4d289ebd395': '2ef65773d541ccaf8ba61a3d141711f0'});
  
  // MOVIES
   initializePlayer('tvn_movies', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/tvNMovies/sa_dash_vmx/tvNMovies.mpd', {'45c004003b09719751adb885d28d491f': 'fe82bc3ffe00477c833812fae74caed7'});
      
  initializePlayer('fmn', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/FMN/sa_dash_vmx/FMN.mpd', {'009a1278e0755ed82e70df01460e90c3': '12d010a918431785676c4fd63ef648bd'});
      
   initializePlayer('hits_movies', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/HitsMovies/sa_dash_vmx/HitsMovies.mpd', {'5d9684b1737e3b68801b4cff7225d4bb': '902e7634820c26a8ff36f3708f0191d4'});
      
  initializePlayer('imc', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/IndonesiaMovieChannels-HD/sa_dash_vmx/IndonesiaMovieChannels-HD.mpd', {'1a551e30ef88a5a121dcea6a74a3aee7': 'b0f3de006d6e31e967a5bc41be086e64'});
      
  initializePlayer('galaxy', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Galaxy-HD/sa_dash_vmx/Galaxy-HD.mpd', {'e5c40292f8d01b1d5f0c6b3904d73104': 'bcb2ac12e11c69594ba217bfe8714efe'});
      
  initializePlayer('galaxy_premium', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/GalaxyPremium-HD/sa_dash_vmx/GalaxyPremium-HD.mpd', {'7bc29ff5f405dff971e3a326223fe26c': '06849a953a38da997b31bacf433cc74a'});    
  initializePlayer('thrill', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Thrill/sa_dash_vmx/Thrill.mpd', {'06e7e95fdb30086d24111f300c9d91f1': '9431050f760f692bfd396bbd84cb5161'});
  
  initializePlayer('cinemaword', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/CinemaWorld/sa_dash_vmx/CinemaWorld.mpd', {'958df746f24f371dffc95e7cd714fbd7': 'fbd6e4540b9a917a27da8be01542f53f'});
  
  initializePlayer('celestial_movies', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/CelestialMovie/sa_dash_vmx/CelestialMovie.mpd', {'13aecb26aeee22bc86920045f22d134d': '80aa44d998c7c0cc221c96d26730fe9b'});
  
  initializePlayer('celestial_clasic_movies', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/CelestialClassic/sa_dash_vmx/CelestialClassic.mpd', {'12da619438b3748da206142216d0943b': '6958377109d34561ad20645319cec62c'});
  
   initializePlayer('zee_bioskop', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/ZeeBIOSKOP/sa_dash_vmx/ZeeBIOSKOP.mpd', {'9e7df70530006bef067327cefde82c0e': 'b92b6a2aba522bd9bb2d07cf241e7398'});
      
  initializePlayer('axn', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/AXN/sa_dash_vmx/AXN.mpd', {'c5d5297a84f8b2de977f07286345c591': 'bdb773d6d25303b75a396b8040cfaf8c'});
      
   initializePlayer('cinemachi', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-HD/sa_dash_vmx/Cinemachi-HD.mpd', {'eeea0c2986e91a13eacd590ad1ebfe43': '0acc16bd84921489f67bad19e4099cae'});
      
  initializePlayer('cinemachi_action', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Action/sa_dash_vmx/Cinemachi-Action.mpd', {'fba45325e2cd8355972ede4981f43b2a': 'd88a4bdd67f89cd21eb074c81de1b994'});
      
  initializePlayer('cinemachi_kids', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Kids-HD/sa_dash_vmx/Cinemachi-Kids-HD.mpd', {'ebe6190a3cfbbed4ec34e4d9c7a30e1e': 'd161b1f737e2aee9501149406c8abe9e'});
      
  initializePlayer('cinemachi_max', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Max-HD/sa_dash_vmx/Cinemachi-Max-HD.mpd', {'6c256f70830647e4bdf654d86e710ed5': 'fe41e9bcbae52f9d233407f7488bca5f'}); 
  
  initializePlayer('cinemachi_xtra', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cinemachi-Xtra-HD/sa_dash_vmx/Cinemachi-Xtra-HD.mpd', {'b64eee6360b118d0d90329cc2c9e4c60': '4e97aa6422b26ffb372647dbd0a7683c'});
  
  initializePlayer('my_cinema', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MyCinema/sa_dash_vmx/MyCinema.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('my_cinema_asia', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MyCinema-Asia/sa_dash_vmx/MyCinema-Asia.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  initializePlayer('my_family', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MyFamily/sa_dash_vmx/MyFamily.mpd', {'4310edb8b9ffe79abb40bacafa778ec3': 'aebb7e86d8a336d9a93d3dd8a41153cf'});
  
  
  //entertainment
  
  
  
  initializePlayer('rock_entertainment', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Rockentertainment/sa_dash_vmx/Rockentertainment.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
  
  initializePlayer('mtv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MTV/sa_dash_vmx/MTV.mpd', {'5b9753643572b0c6c467793b50900029': 'ebeda1cf36dae2b0bdedf1065129ea93'});
  
  initializePlayer('mtv90', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MTV-90-HD/sa_dash_vmx/MTV-90-HD.mpd', {'37ef71f668c1c1126595be0f557587f5': '008df022d78a9ec318a745cf8365b048'});
  
  initializePlayer('celebtv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCInfotainment/sa_dash_vmx/MNCInfotainment.mpd', {'a17bbb3d95ba22f0fa680bba609340f6': 'cb682391dc1830d2cec8ca9d4ce681cc'});
  
  initializePlayer('ent', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCEntertainment/sa_dash_vmx/MNCEntertainment.mpd', {'e97187c5c7978ad9fc48943df7ed9045': '36af3adb3e7c4edd9286e7412fc5f596'});
  
  
  
  initializePlayer('vprime', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/VisionPrime/sa_dash_vmx/VisionPrime.mpd', {'50056951f6d5f5fbaf286cd99c965379': '4ec78e3b7d66d1db3815a127f792b604'});
  
  initializePlayer('musictv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MusicChannel/sa_dash_vmx/MusicChannel.mpd', {'0bea88ab972e06760a04712cc423d841': '4039d607f88f1b1f677e4beb6ee66637'});
  
  initializePlayer('tvn', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/tvN/sa_dash_vmx/tvN.mpd', {'742131dbcb507c31502eb60b32be6bb8': '209f03c525bbbc9fd8652aa7a9cc5cb0'});
  
  initializePlayer('hits', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/HITS/sa_dash_vmx/HITS.mpd', {'78d17d4851a5e9eede96f283b15ec053': 'c5ae33f70cc967fe107b35eb7225f52a'});
  
  initializePlayer('kix', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/KIX/sa_dash_vmx/KIX.mpd', {'fe761e8fac143d40510e70825dad0b20': 'eed9fb0b4e254e9104ef98e8a7035387'});
  
  initializePlayer('kix', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Rockaction/sa_dash_vmx/Rockaction.mpd', {'b9c123fbf587e6524b2706bd960c7f67': 'd2aaac6845b33627cdce1272feac5acc'});
 
  
  //pengetahuan
  initializePlayer('outdoorchannel', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/outdoorchannel/sa_dash_vmx/outdoorchannel.mpd', {'05457a5e16a76fe666a22cc7ae2c6548': '626e37901bfb985e994d714e9f9de1b5'});
  
  initializePlayer('lovenature', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/lovenature/sa_dash_vmx/lovenature.mpd', {'c3066eaa0984588f78d12dcdb75ef88b': '4e98eb4927941bff51ea526d223fa16d'});
  
  initializePlayer('NHKWorldTV', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/NHKWorldTV/sa_dash_vmx/NHKWorldTV.mpd', {'62ce01c83653c3f20af6bcafc6979fe6': 'f1325e71f440c06f7e93d7551cb4014a'});
  
  initializePlayer('BBCEarth', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/BBCEarth-HD/sa_dash_vmx/BBCEarth-HD.mpd', {'c154c12b5f8336cc9cbef82fb5cc0ab4': '5c365427c75eacf217255effcb5fff3e'});
  
  initializePlayer('History', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/History/sa_dash_vmx/History.mpd', {'2ccaa7d9731c2e69fa5344c5a547b836': 'd23fb6a3c73eb7ab8ad19ca841db0fb5'});
  
  initializePlayer('CrimeInvestigation', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/CrimeInvestigation/sa_dash_vmx/CrimeInvestigation.mpd', {'dc6157edc80fa15e6b6f23e695d461e4': '7c068dd99c87e0ab1efc635bb77adcb4'});
  
  initializePlayer('CGTN_Doc', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/CGTN_Doc/sa_dash_vmx/CGTN_Doc.mpd', {'87354c602dcabe0eef472f2a68f6685a': '518ecaa33b9f05494f8cfaa7dd56a968'});
  
  //KIDS 
  initializePlayer('babytv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/BabyTV-NewHD/sa_dash_vmx/BabyTV-NewHD.mpd', {'2fc62ba18b44dce828a5fb8a213d6e5b': '02a01282f77edf03e964e713fb447e93'});
  
  initializePlayer('kidstv', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/KidsChannel/sa_dash_vmx/KidsChannel.mpd', {'17340c4223a4674b1b7da8d8d9e11174': '11a73aad2c75ddf338840b08fb915993'});
  
  initializePlayer('nickeloden', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Nickelodeon/sa_dash_vmx/Nickelodeon.mpd', {'785024598882dc751652cea2bce44b8e': '56a40e58acd59965e865947dd719108d'});
  
  initializePlayer('animax', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Animax/sa_dash_vmx/Animax.mpd', {'6344a8272809245e3fa9d926099528c3': '93b6c4ff420c6864a6294f6d223d9b2c'});
  
  initializePlayer('my_kids', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/My-Kids/sa_dash_vmx/My-Kids.mpd', {'a2dd864fc95998efd52979e5f732e029': '4f705ca934ba3ef5513fea618d4f938d'});
  
  initializePlayer('cebebies', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Cbeebies/sa_dash_vmx/Cbeebies.mpd', {'194891b41223a5f1ec2beff1c95e2f53': 'eeb1fb2ec011f9563653495f9ac427b1'});
  
  initializePlayer('nick_jr', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/NickJr-HDD/sa_dash_vmx/NickJr-HDD.mpd', {'f71ca09f57ea09e27ee63d7d1f0d0e31': '8a07ef45b4a18973ef12f8da482dcec0'});
  
  initializePlayer('My_Kids', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/My-Kids/sa_dash_vmx/My-Kids.mpd', {'a2dd864fc95998efd52979e5f732e029': '4f705ca934ba3ef5513fea618d4f938d'});
  
  initializePlayer('Zoomoo', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Zoomoo/sa_dash_vmx/Zoomoo.mpd', {'5ff6154f28d6950063f9f5b4db7be009': '93cac38fd15124acce5b658896df0a25'});
  
  initializePlayer('Dreamwork', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/Dreamwork-HD/sa_dash_vmx/Dreamwork-HD.mpd', {'57d2ac9210cfbca3596cc679a01c8b29': 'd5e35c0f39c76adf24853d7ea18c71e7'});
  
  
  
  //berita
  
  initializePlayer('MNCnews', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/MNCnews-HDD/sa_dash_vmx/MNCnews-HDD.mpd', {'26cc3e3343858d3986322f16e10b5823': '0ad735e707e1c79bc550efa49f2ea4bd'});
  
  initializePlayer('BeritaSatu', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/BeritaSatu/sa_dash_vmx/BeritaSatu.mpd', {'d022ff3be9391a16a8436261059806b0': 'e6dd32d6a257f900acb52a82d898bb81'});
  
  initializePlayer('IDX', 'https://allo.xyssatuu.workers.dev/https://xxx.livestreamingbolatv1.workers.dev/https://xxx.freemiumiptv.top/mnclokal/live/eds/IDX/sa_dash_vmx/IDX.mpd', {'fbdbe252e6354b175852d1929c42d848': '7a8bb14fcf8fd0b896ab41c29a78aae0'});
      
      
     
    });